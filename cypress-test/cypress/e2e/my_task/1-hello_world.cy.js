/// <reference types="cypress" />

// describe("First test", () => {
//   // it("test one", () => {
//   //   cy.visit("https://codedamn.com");

//   //   // way 1
//   //   cy.contains("Build projects");
//   //   cy.get("[data-testid=menutoggle]").click();

//   //   // way 2
//   //   cy.get(".asyncComponent > div > a");

//   //   // way 3
//   //   cy.get("[data-test-id=learnbtn");
//   // });

//   // it("Every basic element exists", () => {
//   //   cy.viewport(1280, 720);
//   //   cy.visit("https://codedamn.com");
//   // });

//   // it("Every basic element exists on mobile", () => {
//   //   cy.viewport("iphone-x");
//   //   cy.visit("https://codedamn.com");
//   // });

//   // it.only("Login page looks good", () => {
//   //   cy.viewport(1280, 720);
//   //   cy.visit("https://codedamn.com");

//   //   cy.contains("Sign in").click();
//   //   cy.contains("Sign in").should("exist");
//   //   cy.contains("Sign in with Google").should("exist");
//   //   cy.contains("Sign in with GitHub").should("exist");
//   //   cy.contains("Forgot your password?").should("exist");
//   //   cy.contains("Create one").should("exist");
//   // });

//   it("The login page links work", () => {
//     cy.viewport(1280, 720);
//     cy.visit("https://codedamn.com");

//     // 1. sign in page
//     cy.contains("Sign in").click();

//     // 2. password reset page
//     cy.contains("Forgot your password?").click({ force: true });

//     // 3. verify page URL
//     cy.url().should("include", "/password-reset");

//     // 4. go back to the sign in page
//     cy.go("back");

//     // 5. register account page
//     cy.contains("Create one").click({ force: true });

//     // 6. verify page URL
//     cy.url().should("include", "/register");
//   });

//   it.only("Login should work fine", () => {
//     cy.viewport(1280, 720);
//     cy.visit("https://codedamn.com");

//     cy.contains("Sign in").click();

//     cy.contains("Unable to authorize").should("not.exist");
//     cy.get("[data-testid='username']").type("admin", { force: true });
//     cy.get("[data-testid='password']").type("admin", { force: true });

//     cy.get("[data-testid='login']").click({ force: true });
//     cy.contains("Unable to authorize").should("exist");
//   });
// });

// token
// const token = "";
// // desktop tes local storage
// describe("Basic Desktop Test", () => {
//   before(() => {
//     cy.then(() => {
//       window.localStorage.setItem("__auth__token", token);
//     });
//   });

//   beforeEach(() => {
//     cy.viewport(1280, 720);

//     cy.visit("https://codedamn.com");
//   });

//   it("Should pass", () => {});
// });

describe("Playground test", () => {
    it("Should load playground correctly", () => {
      cy.visit("https://codedamn.com/playground/html");
  
      cy.log("Checking for sidebar");
      cy.contains("Trying to connect").should("exist");
  
      cy.log("Checking bottom left bottom");
      cy.get("[data-testid=xterm-controls] > div").should(
        "contain.text",
        "Connecting"
      );
  
      // cy.debug();
      cy.contains("Trying to establish connection").should("exist");
  
      cy.log("Playground is initializing");
  
      cy.contains("Setting up the challenge").should("exist");
      // set timeout to 10 seconds
      cy.contains("Setting up the challenge", { timeout: 10 * 1000 }).should(
        "not.exist"
      );
    });
  
    it.only("New file feature works", () => {
      cy.visit("https://codedamn.com/playground/html");
  
      cy.contains("Setting up the challenge", { timeout: 7 * 1000 }).should(
        "exist"
      );
      cy.contains("Setting up the challenge", { timeout: 7 * 1000 }).should(
        "not.exist"
      );
  
      const fileName = Math.random();
  
      cy.get("[data-testid=xterm]")
        .type("{ctrl]{c}")
        .type(`touch testscript.${fileName}.js{enter}`);
  
      cy.contains(`testscript.${fileName}.js`).rightclick();
  
      cy.contains("Rename File").click();
  
      cy.get("[data-testid=renamefilefolder]").type(`new_.${fileName}.js`);
  
      cy.contains(`testscript.${fileName}.js`).should("not.exist");
      cy.contains(`new_.${fileName}.js`).should("exist");
    });
  });
  

